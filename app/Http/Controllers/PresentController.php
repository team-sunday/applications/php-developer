<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;

class PresentController extends BaseController
{
    public function request() {
      return view('present.request');
    }
    
    public function manage() {
      return view('present.manage');
    }
}
