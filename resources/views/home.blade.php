@extends('layouts/skeleton')

@section('body')
  <div class="flex-center position-ref full-height">
      <div class="content">
          <div class="title m-b-md">
              PHP-Developer
          </div>

          <h2>Present</h2>
          <div class="links">
              <a href="{{ Route('present.request') }}">Request</a>
              <a href="{{ Route('present.manage') }}">Manage</a>
          </div>
      </div>
  </div>
@endsection
