<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@home')->name('welcome');
Route::get('/dashboard', 'Controller@home')->name('home');
Route::get('/present', 'PresentController@request')->name('present.request');
Route::get('/present/manage', 'PresentController@manage')->name('present.manage');
